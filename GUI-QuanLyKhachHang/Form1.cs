﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace GUI_QuanLyKhachHang
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string sqlConn = @"Server=DESKTOP-IIQ7LRK\SQLEXPRESS;Database=CSDL_QLKH;Integrated Security = True";
        SqlConnection conn = null;

        private void OpenConnection()
        {
            if (conn == null)
                conn = new SqlConnection(sqlConn);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
        }

        private void ClosedConnection()
        {
            if (conn != null && conn.State == ConnectionState.Open)
                conn.Close();
        }

        private void HienThiToanBoKhachHang()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from KhachHang";
                command.Connection = conn;
                SqlDataReader reader = command.ExecuteReader();
                lvKhachHang.Items.Clear();
                lvKhachHang.Groups.Clear();
                ListViewGroup lvgVip = new ListViewGroup("Khách hàng VIP");
                lvKhachHang.Groups.Add(lvgVip);
                ListViewGroup lvgThuong = new ListViewGroup("Khách hàng Thường");
                lvKhachHang.Groups.Add(lvgThuong);
                int stt = 0;
                while (reader.Read())
                {
                    int ma = reader.GetInt32(0);
                    string ten = reader.GetString(1);
                    int gioitinh = reader.GetInt32(2);
                    string phone = reader.GetString(3);
                    int loaiKH = reader.GetInt32(4);
                    stt++;
                    ListViewItem lvi = new ListViewItem((stt) + "");
                    lvi.SubItems.Add(ma + "");
                    lvi.SubItems.Add(ten);
                    lvi.SubItems.Add(gioitinh == 0 ? "Nam" : "Nữ");
                    lvi.SubItems.Add(phone);
                    if (loaiKH == 1)
                        lvi.Group = lvgVip;
                    else
                        lvi.Group = lvgThuong;
                    lvKhachHang.Items.Add(lvi);
                    if (gioitinh == 0)
                        lvi.ImageIndex = 0;
                    else
                        lvi.ImageIndex = 1;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            HienThiToanBoKhachHang();
        }

        private void HienThiChiTiet(int ma)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from KhachHang where Ma=@ma";
                command.Connection = conn;
                SqlParameter parameter = new SqlParameter("@ma", SqlDbType.Int);
                parameter.Value = ma;
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    string ten = reader.GetString(1);
                    int gioitinh = reader.GetInt32(2);
                    string phone = reader.GetString(3);
                    int loai = reader.GetInt32(4);
                    txtMa.Text = ma + "";
                    txtTen.Text = ten;
                    if (gioitinh == 0)
                        radNam.Checked = true;
                    else
                        radNu.Checked = true;
                    txtPhone.Text = phone;
                    if (loai == 1)
                        cboLoaiKhachHang.SelectedIndex = 0;
                    else
                        cboLoaiKhachHang.SelectedIndex = 1;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lvKhachHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvKhachHang.SelectedItems.Count == 0) return;
            if (lvKhachHang.SelectedItems.Count > 0)
            {
                ListViewItem lvi = lvKhachHang.SelectedItems[0];
                int ma = int.Parse(lvi.Text);
                //int ma = int.Parse(lvKhachHang.SelectedItems[0].SubItems[0].Text);
                HienThiChiTiet(ma);
            }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtPhone.Text = "";
            radNam.Checked = false;
            radNu.Checked = false;
            cboLoaiKhachHang.SelectedIndex = -1;
            txtMa.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int ma = int.Parse(txtMa.Text);
            if (KiemTraTonTai(ma))
            {
                //tiến hành cập nhật
                CapNhatKhachHang(ma);
            }
            else
            {
                // tiến hành thêm mới
                ThemMoiKhachHang();
            }
        }

        private void CapNhatKhachHang(int ma)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                string sql = "update KhachHang set Ten=@ten,GioiTinh=@gioitinh,Phone=@phone,LoaiKH=@loaiKH where Ma=@ma";
                command.CommandText = sql;
                command.Connection = conn;
                command.Parameters.Add("@ten", SqlDbType.NVarChar).Value = txtTen.Text;
                command.Parameters.Add("@gioitinh", SqlDbType.Int).Value = radNam.Checked ? 0 : 1;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = txtPhone.Text;
                command.Parameters.Add("@loaiKH", SqlDbType.Int).Value = (cboLoaiKhachHang.Text == "VIP") ? 1 : 0;
                command.Parameters.Add("@ma", SqlDbType.Int).Value = txtMa.Text;
                int kq = command.ExecuteNonQuery();
                if (kq>0)
                {
                    HienThiToanBoKhachHang();
                    MessageBox.Show("Cập nhật thành công !");
                    btnTaoMoi.PerformClick();
                }
                else
                {
                    MessageBox.Show("Cập nhật thất bại !");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ThemMoiKhachHang()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                string sql = "insert into KhachHang values(@ma,@ten,@gioitinh,@phone,@loaiKH)";
                command.CommandText = sql;
                command.Connection = conn;
                command.Parameters.Add("@ma", SqlDbType.Int).Value = txtMa.Text;
                command.Parameters.Add("@ten", SqlDbType.NVarChar).Value = txtTen.Text;
                if (radNam.Checked == true)
                    command.Parameters.Add("@gioitinh", SqlDbType.Int).Value = 0;
                else
                    command.Parameters.Add("@gioitinh", SqlDbType.Int).Value = 1;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = txtPhone.Text;
                if (cboLoaiKhachHang.Text == "VIP")
                    command.Parameters.Add("@loaiKH", SqlDbType.Int).Value = 1;
                else
                    command.Parameters.Add("@loaiKH", SqlDbType.Int).Value = 0;
                int kq = command.ExecuteNonQuery();
                if (kq>0)
                {
                    HienThiToanBoKhachHang();
                    MessageBox.Show("Lưu thông tin thành công !");
                }
                else
                {
                    MessageBox.Show("Lưu thông tin thất bại !");
                }
                btnTaoMoi.PerformClick();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        bool KiemTraTonTai(int ma)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from KhachHang where Ma=@ma";
                command.Connection = conn;
                SqlParameter parameter = new SqlParameter("@ma", SqlDbType.Int);
                parameter.Value = ma;
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();
                bool kq = reader.Read();
                reader.Close();
                return kq;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lvKhachHang.SelectedItems.Count == 0) return;
            ListViewItem lvi = lvKhachHang.SelectedItems[0];
            int ma = int.Parse(lvi.Text);
            DialogResult ret = MessageBox.Show($"Bạn có chắc chắn muốn xóa khách hàng có mã = [{ma}] không ?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ret == DialogResult.Yes)
            {
                ThucHienXoa(ma);
            }
        }

        private void ThucHienXoa(int ma)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "delete from KhachHang where Ma=@ma";
                command.Connection = conn;
                command.Parameters.Add("@ma", SqlDbType.Int).Value = ma;
                int kq = command.ExecuteNonQuery();
                if (kq > 0)
                {
                    HienThiToanBoKhachHang();
                    MessageBox.Show("Xóa khách hàng thành công !");
                }
                else
                {
                    MessageBox.Show("Xóa khách hàng thất bại !");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
